**Воробьев Леонид**
**КИ22-16/1Б**
**_Вариант 10_**

Функции в программе:

* print_field(field)
* move(start, field)
* moves(steps, field)
* knight_move(start, finish)
* knights_collision(start, finish)

```
def knight_move(start, finish):
    field = []
    for _ in range(8):
        row = []
        for _ in range(8):
            row.append(256)
        field.append(row)
    field[start[1]-1][start[0]-1] = 0
    steps = 0
    while True:
        field = moves(steps, field)
        steps += 1
        if field[finish[1]-1][finish[0]-1] != 256:
            return field[finish[1]-1][finish[0]-1]
```

`$ python main.py --help` — вызов интерфейса для демонстрации работы

`$ python main.py --start 2 3 --finish 2 8` — вычислить минимальное число ходов коня от точки до точки

`$ python main.py --first 2 3 --second 2 8` — вычислить минимальное кол-во ходов, за которое два коня могут встретиться


**Выбери свой класс:**
* [ ] Воин
* [ ] Маг
* [ ] Клирик
* [x] ПИшник


![я и мой друг Захар](https://cdn.idaprikol.ru/images/bb57b4ec23202af1415965eb4444b43cbd48a5e6f5c204727f8e5a01ff9dc626_1.webp)

[пин на максималках](https://www.youtube.com/watch?v=urMo0COpCo8)

